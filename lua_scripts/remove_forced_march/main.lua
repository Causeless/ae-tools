--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- main.lua
-- Handles removing forced march / double time modifications

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local scripting = require "lua_scripts.EpisodicScripting"
local events = require "lua_scripts.custom_events"
local ui = require "lua_scripts.ui"

events.AddEventCallBack("CharacterSelected", 
    function() 
        local army_stance_group = scripting.m_root:Find("land_stance_button_stack")
        ui.removeUIComponent( UIComponent(army_stance_group):Find("button_forced_march") )
    end
) -- Remove forced march button upon character selected

events.AddEventCallBack("CharacterSelected", 
    function() 
        local navy_stance_group = scripting.m_root:Find("naval_stance_button_stack")
        ui.removeUIComponent( UIComponent(navy_stance_group):Find("button_double_time") )
    end
) -- Remove double time button upon character selected

dev.log("remove_forced_march loaded")