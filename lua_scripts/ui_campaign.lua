--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- ui_campaign.lua
-- Handles main menu user interface related modifications

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"

function showMessageToHumans(title, primaryDetail, secondaryDetail)
    local faction_list = cm:model():world():faction_list()
    for i = 0, faction_list:num_items() - 1 do
        local current_faction = faction_list:item_at(i)
            
        if not current_faction:is_null_interface() and current_faction:is_human() then
            cm:show_message_event(
                current_faction:name(),
                title,
                primaryDetail,
                secondaryDetail,
                true,
                667
            )
        end
    end
end

dev.log("ui_campaign.lua loaded")