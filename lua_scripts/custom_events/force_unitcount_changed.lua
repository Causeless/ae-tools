--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- force_unitcount_changed.lua
-- Event which is called whenever a military force's unit count changes

-- this line needs to be present - due to the way this script file is loaded by custom_events.lua it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.custom_events_env)

local dev = require "lua_scripts.dev"
local util_faction = require "lua_scripts.faction"

createEvent("ForceUnitCountChanged")

_forceInfo = {}
local function callForceUnitCountChangedCallbacks(forceCQI, tab)
    for key, func in getEventCallbacks("ForceUnitCountChanged") do
        func( { 
                ["forceCQI"] = forceCQI,
                ["regionKey"] = tab.regionKey
            } )
    end
end

local function checkForForceUnitCountChanged()
    -- We only check military forces for the current faction
    -- This is technically inaccurate but is good enough for what we need it for, and a lot faster than checking every faction
    if not util_faction.isDuringFactionTurn() then return end
    local forceList = util_faction.getCurrentFaction():military_force_list()

    for j = 0, forceList:num_items() - 1 do
        local force = forceList:item_at(j)
        local forceCQI = force:command_queue_index()

        local unitCount = force:unit_list():num_items()
        local character = force:general_character()

        local regionKey = nil
        if character:has_region() then
            regionKey = character:region():name()
        end
        
        if not _forceInfo[forceCQI] then _forceInfo[forceCQI] = {["unitCount"] = unitCount, ["regionKey"] = regionKey} end
        local tab = _forceInfo[forceCQI]

        if unitCount ~= tab.unitCount then
            callForceUnitCountChangedCallbacks(forceCQI, tab)
            
            tab.unitCount = unitCount
            tab.regionKey = regionKey
        end
    end
end

AddEventCallBack("WorldCreated", function()
    AddEventCallBack("SimTick", checkForForceUnitCountChanged)
end)

dev.log("custom_events.force_unitcount_changed.lua loaded")