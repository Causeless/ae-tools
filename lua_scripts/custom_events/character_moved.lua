--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- character_moved.lua
-- Event which is called whenever a character moves

-- this line needs to be present - due to the way this script file is loaded by custom_events.lua it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.custom_events_env)

local dev = require "lua_scripts.dev"
local util_faction = require "lua_scripts.faction"

createEvent("CharacterMoved")

_charInfo = {}
local function callCharacterMoveCallbacks(charCQI, tab)
    for key, func in getEventCallbacks("CharacterMoved") do
        func( {
                ["charCQI"] = charCQI, 
                ["regionKey"] = tab.regionKey, 
                ["posX"] = tab.posX, 
                ["posY"] = tab.posY
            } )
    end
end

local function checkForCharacterMovement()
    -- We assume characters can only move if it's their faction's turn
    -- This is technically inaccurate - a faction can retreat their force during another faction's turn, when attacked
    -- But this is good enough for what we need it for, and a lot faster than checking every faction's characters
    if not util_faction.isDuringFactionTurn() then return end
    local charList = util_faction.getCurrentFaction():character_list()

    for j = 0, charList:num_items() - 1 do
        local character = charList:item_at(j)
        local charCQI = character:cqi()
        
        local posX = character:display_position_x()
        local posY = character:display_position_y()
        
        local regionKey = nil
        if character:has_region() then
            regionKey = character:region():name()
        end
        
        if not _charInfo[charCQI] then _charInfo[charCQI] = {["posX"] = posX, ["posY"] = posY, ["regionKey"] = regionKey} end
        local tab = _charInfo[charCQI]
        
        if posX ~= tab.posX or posY ~= tab.posY then
            callCharacterMoveCallbacks(charCQI, tab)
            
            tab.posX = posX
            tab.posY = posY
            tab.regionKey = regionKey
        end
    end
end

AddEventCallBack("WorldCreated", function()
    AddEventCallBack("SimTick", checkForCharacterMovement)
end)

dev.log("custom_events.character_moved.lua loaded")