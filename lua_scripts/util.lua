--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- util.lua
-- Handles utility functions, wrapper functions etc

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"

-- IO stuff
function fileExists(filepath)
    local fileCheck = io.open(filepath, "r")
    
    if not fileCheck then 
        fileCheck = false
    else 
        fileCheck:close()
        fileCheck = true
    end
    
    return fileCheck
end

-- String stuff
-- Encodes a string in a safe-to-store way
function encodeString(s)
    return (string.gsub(s, "([\\|=])", 
        function (x)
            return string.format("\\%03d", string.byte(x))
        end)
    )
end

-- Decodes a previously encoded string
function decodeString(s)
    return (string.gsub(s, "\\(%d%d%d)", 
        function (d)
            return string.char(d)
        end)
    )
end

-- Splits a string on delim
-- splitString("test=test1=test2"), "=") would return
-- { "test", "test1", "test2" }
function splitString(inputStr, delim)
    local t={} ; i=1
    for str in string.gmatch(inputStr, "([^"..delim.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

-- Returns character at position X in str
function indexString(str, x)
    return string.sub(str, x, x)
end

-- Convert string to boolean
function toboolean(str)
    if str == "true" then
        return true
    elseif str == "false" then
        return false
    end

    -- Invalid string
    dev.log("ERROR: toboolean()\n    '"..tostring(str).. "' is not a recognized boolean value")
    return false -- Assume false
end

-- Saving/loading

local function convertSigilToType(str)
    if str == "$" then
        return "number"
    elseif str == "@" then
        return "string"
    elseif str == "#" then
        return "boolean"
    end

    -- Invalid sigil
    dev.log("ERROR: convertSigilToType()\n    '"..tostring(str).. "' is not a recognized sigil")
    return "string" -- Assume string
end

-- This function will deeply save all sub-tables recursively and generically
-- This requires the context from the saving game callback, the table to be saved, and a savename
function deepSaveTable(context, tb, savename)
    local savestring = ""

    -- Cache functions for performance
    local encode = encodeString
    local index = indexString

    for rawKey, value in pairs(tb) do
        local key;
        local continueSave = true;

        -- Sometimes we don't want to save all data in a table, for example for weak references, or to avoid cycles
        -- For this, we have a little workaround - keys where where the first character is "_" are not saved 
        if index(rawKey, 0) == "_" then
            continueSave = false
        end

        -- Keys can be strings (and so can include illegal characters), so encode
        if type(rawKey) == "string" then
            -- Preprend with @ to signify as a string
            key = "@"..encode(rawKey)
        elseif type(rawKey) == "number" then
            -- Preprend with $ to signify as a number
            key = "$"..tostring(rawKey)
        elseif type(rawKey) == "boolean" then
            -- Preprend with # to signify as a boolean
            key = "#"..tostring(rawKey)
        else
            -- Invalid type
            dev.log("WARNING: deepSaveTable()\n    key '"..rawKey.. "' is of type '"..type(rawKey).."', a non saveable type. This will not be saved.")
            continueSave = false
        end

        if continueSave then
            if type(value) == "table" then
                -- We're saving a table, so recurse
                -- We save the new table's name delimited by a ">"
                deepSaveTable(context, value, savename..">"..key)
                -- And save as a key on the parent table so we can find the new save name when loading
                savestring = savestring..key.."|"
            elseif type(value) == "string" then
                -- Preprend with @ to signify as a string
                -- And encode to avoid issues with parse characters
                savestring = savestring..key.."=@"..encode(value).."|"
            elseif type(value) == "number" then
                -- Preprend with $ to signify as a number
                savestring = savestring..key.."=$"..value.."|"
            elseif type(value) == "boolean" then
                -- Preprend with # to signify as a boolean
                savestring = savestring..key.."=#"..tostring(value).."|"
            else
                -- Invalid type
                dev.log("WARNING: deepSaveTable()\n    value for '"..rawKey.. "' is of type '"..type(value).."', a non saveable type. This will not be saved.")
            end
        end
    end
    -- Don't use cm:save_value, so we don't clog up logging file
    scripting.game_interface:save_named_value(savename, savestring, context)
end

-- Loads and returns a previously saved table
-- This function will deeply load all sub-tables recursively and generically
-- This requires the context from the loading game callback and the savename
function deepLoadTable(context, savename)
    -- Don't use cm:load_value, so we don't clog up logging file
    local savestring = scripting.game_interface:load_named_value(savename, "", context)
    local tb = {}
    
    if savestring ~= "" then
        -- Cache functions for performance
        local decode = decodeString
        local splitStr = splitString
        local index = indexString
        local sub = string.sub
        local cnv = convertSigilToType

        local split = splitStr(savestring, "|")

        for i = 1, #split do                                 
            local valueSplit = splitStr(split[i], "=")
            
            local keyType = cnv(index(valueSplit[1], 1))
            local rawKey = valueSplit[1]
            local key = sub(valueSplit[1], 2)

            local valueType, value;
            if valueSplit[2] then
                valueType = cnv(index(valueSplit[2], 1))
                value = sub(valueSplit[2], 2)
            end
            
            if keyType == "number" then
                key = tonumber(key)
            elseif keyType == "string" then
                key = decode(key)
            elseif keyType == "boolean" then
                key = toboolean(key)
            end
            
            if not value then
                -- We're loading a table, so recurse
                tb[key] = deepLoadTable(context, savename..">"..rawKey)
            elseif valueType == "number" then
                tb[key] = tonumber(value)
            elseif valueType == "string" then
                tb[key] = decode(value)
            elseif valueType == "boolean" then
                tb[key] = toboolean(value)
            end
        end
    end

    return tb
end

-- Converts a table and sub-tables into a valid lua code representation
-- indent is not designed to be used as an argument, as it only for internal usage while recursing
function stringifyTable(tb, tbName, indent)
    indent = indent or ""

    local str = indent..tbName.." = {\n"
    for key, value in pairs(tb) do
        if type(key) == "string" then
            key = "\""..key.."\""
        elseif type(key) == "boolean" then
            key = tostring(key)
        end

        if type(value) == "table" then
            -- We're printing a table, so recurse
            str = str..stringifyTable(value, key, indent.."    ")..",\n"
        elseif type(value) == "string" then
            str = str..indent.."    "..key.." = \""..value.."\",\n"
        elseif type(value) == "boolean" then
            str = str..indent.."    "..key.." = "..tostring(value)..",\n"
        else
            -- Print numbers without being surrounded by quotes
            str = str..indent.."    "..key.." = "..tostring(value)..",\n"
        end
    end
    -- Now remove the last trailing comma to make it a valid lua table
    str = string.gsub(str, ",\n$", "\n")
    return str..indent.."}"
end

-- Prints a table and all of it's sub-tables
-- The second argument, tbName, is optional and will print the top-level table name in the output if given
-- Child table names are always printed
function deepPrintTable(tb, tbName)
    dev.log(stringifyTable(tb, tbName or ""))
end

function deepCopy(obj, seen)
    if type(obj) ~= 'table' then return obj end
    if seen and seen[obj] then return seen[obj] end

    local s = seen or {}
    local res = setmetatable({}, getmetatable(obj))
    s[obj] = res
    for k, v in pairs(obj) do res[deepCopy(k, s)] = deepCopy(v, s) end
    return res
end

function shallowCopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end