--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- growth_manager.lua
-- Handles region populations

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"

local pop = require "lua_scripts.manpower.population_manager"

local function FactionTurnEnd(context)
    local faction = context:faction()
    local regions = faction:region_list()
    
    if regions:is_empty() then return end
    
    -- we should actually loop through Region population objects, not literal regions, to perform growth for hordes too
    for i = 0, regions:num_items()-1 do
        local region = regions:item_at(i)
        local regionKey = region:name()
        
        -- TODO: Growth is disabled for now, as we implement multiple classes
        --local population = pop.getPopulations(regionKey)
        --pop.setRegionPopulation(regionKey, (population + STATIC_GROWTH) * GROWTH_RATE)
        -- Grow region population every turn. Later, the growth count ought to change due to food/immigrants or whatever, and be far deeper and more complex
    end
end

-- Callbacks
events.AddEventCallBack("FactionTurnEnd", FactionTurnEnd)

-- Logging
dev.log("manpower.growth_manager.lua loaded")