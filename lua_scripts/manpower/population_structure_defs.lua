--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- structure_defs.lua
-- Defines structures for population classes and cultures

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

local unit_interface = require "lua_scripts.manpower.unit_interface"
local static = require "scripts_libs_mod.lib_manpower_static_entities"
local dev = require "lua_scripts.dev"

require "lua_scripts.third_party.class"

-- Defines a population class (rich, poor, etc)
class "PopulationClass"
function PopulationClass:PopulationClass(classVal, classConfig)
    self._config = classConfig
    self.population = classVal.population or classConfig.defaultPopulation
end
 
function PopulationClass:getDisplayName()
    return self._config.displayName
end

-- Defines a population culture (roman, greek, etc)
class "PopulationCulture"
function PopulationCulture:PopulationCulture(cultureVal, cultureConfig)
    self._config = cultureConfig
    self.classes = {}

    -- Initialize
    for classKey, classConfig in pairs(cultureConfig.classes) do
        local classVal = {}
        if cultureVal.classes and cultureVal.classes[classKey] then 
            classVal = cultureVal.classes[classKey]
        end

        self.classes[classKey] = PopulationClass(classVal, classConfig)
    end
end

function PopulationCulture:getDisplayName()
    return self._config.displayName
end

function PopulationCulture:getTotalPopulation()
    local totalPopulation = 0

    for key, class in pairs(self.classes) do
        totalPopulation = totalPopulation + class.population
    end

    return totalPopulation
end

-- Defines a population source (region, horde)
class "PopulationSource"
function PopulationSource:PopulationSource(obj)
    obj = obj or {}
    self.cultures = {}

    -- Initialize from static entities
    for cultureKey, cultureConfig in pairs(static.cultures) do
        local cultureVal = {}
        if obj.cultures and obj.cultures[cultureKey] then 
            cultureVal = obj.cultures[cultureKey]
        end

        self.cultures[cultureKey] = PopulationCulture(cultureVal, cultureConfig)
    end
end

function PopulationSource:getPopulationCultureForUnit(unitKey)
    local unitCulture = unit_interface.getUnitCulture(unitKey)
    return self.cultures[unitCulture]
end

function PopulationSource:getPopulationClassForUnit(unitKey)
    local unitClass = unit_interface.getUnitClass(unitKey)
    local culture = self:getPopulationCultureForUnit(unitKey)

    return culture.classes[unitClass]
end

function PopulationSource:getTotalPopulation()
    local totalPopulation = 0

    for key, culture in pairs(self.cultures) do
        totalPopulation = totalPopulation + culture:getTotalPopulation()
    end

    return totalPopulation
end

-- Logging
dev.log("manpower.population_structure_defs.lua loaded")