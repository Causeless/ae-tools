--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- population_manager.lua
-- Handles population sources

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local util = require "lua_scripts.util"

local entities = require "lua_scripts.manpower.population_structure_defs"
local pop_data = require "scripts_libs_mod.lib_manpower_populations"
local provinces = require "scripts_libs_tools.lib_regions_to_provinces"

-- Constants
-- Max population for a population classe
-- This probably ought to be changed to be a customizable value per-class
MAX_POPULATION = 8000

-- Saved / loaded stuff

-- This distinction between region populations is required for MP support
-- m_volatilePopulations is valid to be edited by UI events which aren't synced across multiplayer
-- For example, reducing it when a player clicks a unit recruitment card, so they get instant feedback on the population change
-- The real population can only be modified by actions synced across MP, unless you want a desync
-- These same tables hold both region and horde population - all population sources
m_volatilePopulations = {} -- This is the UI-side population of each region
m_syncedPopulations = {} -- This is the "real" population of each region. This is synced between players in MP.

-- Fix, seems horde populations aren't being synced properly?
function initializePopulationSources()
    dev.log("Initializing population sources")
    if newGame then
        m_syncedPopulations = deserializePopulationSources(pop_data.populations) -- set it to the default values
    end

    for regionKey, provinceKey in pairs(provinces.fromRegionKey) do
        -- For any default values not found, just create a new empty default PopulationSource for the region
        if not m_syncedPopulations[regionKey] then
            m_syncedPopulations[regionKey] = entities.PopulationSource()
        end
    end

    -- Same goes for horde forces
    -- TODO: think about being careful when a faction settles
    -- Or when a horde is destroyed. or when a scripted force is created. or when a new horde is created...
    local factions = cm:model():world():faction_list()
    if factions:is_empty() then return end
    
    for i = 0, factions:num_items()-1 do
        local faction = factions:item_at(i)
        
        if faction:is_horde() then
            createPopulationSourceForHorde(faction)
        end
    end

    if newgame then
        m_volatilePopulations = util.deepCopy(m_syncedPopulations)
    end
end

-- Gets the population source for an force
-- A normal force takes population from the region it is in, while a horde stores it's own population
-- This can take an option secondary param regionKey, which is used for MP support. See more info in force_state_manager's updateForce
function getPopulationSource(forceCQI, regionKey)
    local force = cm:model():military_force_for_command_queue_index(forceCQI)

    if force:is_horde() then
        return "force_"..forceCQI
    elseif regionKey then
        return regionKey
    else
        return force:general_character():region():name()
    end
end

function getHordePopulationSource(forceCQI)
    return "force_"..forceCQI
end

local function ScriptedForceCreated(context)
    -- If a scripted force is created for a horde, create the population source for it
    local force = context:military_force()
    local popSourceKey = getPopulationSource(force:command_queue_index())

    if not force:is_navy() and not m_syncedPopulations[popSourceKey] then
        m_syncedPopulations[popSourceKey] = entities.PopulationSource()
    end
end


function createPopulationSourceForHorde(faction)
    -- Create a population source for each of this faction's forces
    local forces = faction:military_force_list()
    if not forces:is_empty() then
        for i = 0, forces:num_items()-1 do
            local force = forces:item_at(i)
            local popSourceKey = getPopulationSource(force:command_queue_index())

            if not force:is_navy() and not m_syncedPopulations[popSourceKey] then
                m_syncedPopulations[popSourceKey] = entities.PopulationSource()
            end
        end
    end
end

-- Used for hordes, to remove their population source if the force dissapears
function destroyPopulationSource(popSourceKey)
    if m_syncedPopulations[popSourceKey] then
        m_syncedPopulations[popSourceKey] = nil;
    end

    if m_volatilePopulations[popSourceKey] then
        m_volatilePopulations[popSourceKey] = nil;
    end
end

function deserializePopulationSources(populationsPOD)
    local populations = {}

    for popSourceKey, popSourceData in pairs(populationsPOD) do
        populations[popSourceKey] = entities.PopulationSource(popSourceData)
    end

    return populations
end

-- TODO: Fix this! this means AI disbanding does not properly change population since real pop goes above ui
function resetUIPopulation()
    for popSourceKey, popSource in pairs(m_syncedPopulations) do
        if m_volatilePopulations[popSourceKey] == nil then
            m_volatilePopulations[popSourceKey] = util.deepCopy(popSource)
        end

        for cultureKey, culture in pairs(popSource.cultures) do
            for classKey, realClass in pairs(culture.classes) do
                local volatileClass = m_volatilePopulations[popSourceKey].cultures[cultureKey].classes[classKey]
                volatileClass.population = math.min(volatileClass.population, realClass.population)
            end
        end
    end

    dev.log("updating UI population with real population")
end

-- m_syncedPopulations stuff
function addManpowerForUnit(popSourceKey, amount, unitKey)
    --dev.log("Adding " .. amount .. " population to " .. popSourceKey)
    local popClass = m_syncedPopulations[popSourceKey]:getPopulationClassForUnit(unitKey)

    popClass.population = math.floor(popClass.population + amount)
    popClass.population = math.min(popClass.population, MAX_POPULATION)
    
    --dev.log(popSourceKey .. " now has a population of " .. m_syncedPopulations[popSourceKey])
end

-- Tally total population of population source
function getTotalPopulation(popSourceKey)
    return m_syncedPopulations[popSourceKey]:getTotalPopulation()
end

-- Get a population source
function getPopulations(popSourceKey)
    return m_syncedPopulations[popSourceKey]
end

-- Get available manpower for a unit
function getManpowerForUnit(popSourceKey, unitKey)
    return m_syncedPopulations[popSourceKey]:getPopulationClassForUnit(unitKey).population
end

-- m_volatilePopulations stuff
function UIaddManpowerForUnit(popSourceKey, amount, unitKey)
    local popClass = m_volatilePopulations[popSourceKey]:getPopulationClassForUnit(unitKey)

    popClass.population = math.floor(popClass.population + amount)
    popClass.population = math.min(popClass.population, MAX_POPULATION)
end

-- Tally total population of population source
function UIgetTotalPopulation(popSourceKey)
    return m_volatilePopulations[popSourceKey]:getTotalPopulation()
end

function UIgetPopulations(popSourceKey)
    return m_volatilePopulations[popSourceKey]
end

function UIgetManpowerForUnit(popSourceKey, unitKey)
    return m_volatilePopulations[popSourceKey]:getPopulationClassForUnit(unitKey).population
end

------------------------------------------------
---------------- Saving/Loading ----------------
------------------------------------------------

cm:register_loading_game_callback(
    function(context)
        -- We must convert from the POD to the actual types here
        m_syncedPopulations = deserializePopulationSources( util.deepLoadTable(context, "m_syncedPopulations") )
        m_volatilePopulations = deserializePopulationSources( util.deepLoadTable(context, "m_volatilePopulations") )
    end
)
cm:register_saving_game_callback(
    function(context)
        util.deepSaveTable(context, m_syncedPopulations, "m_syncedPopulations")
        util.deepSaveTable(context, m_volatilePopulations, "m_volatilePopulations")
    end
)

-- Logging
dev.log("manpower.population_manager.lua loaded")