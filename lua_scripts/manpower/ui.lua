--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- ui.lua
-- Handles UI modifications for manpower

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local util = require "lua_scripts.util"
local util_region = require "lua_scripts.region"
local util_faction = require "lua_scripts.faction"
local ui = require "lua_scripts.ui"
local ui_recruitment = require "lua_scripts.ui_recruitment"
local ui_campaign = require "lua_scripts.ui_campaign"
local events = require "lua_scripts.custom_events"

local unit_interface = require "lua_scripts.manpower.unit_interface"
local pop = require "lua_scripts.manpower.population_manager"

local provinces = require "scripts_libs_tools.lib_regions_to_provinces"
local regions = require "scripts_libs_tools.lib_provinces_to_regions"

m_currentRegionKey = nil

function showManpowerIntroToAllFactions()
    ui_campaign.showMessageToHumans("event_feed_manpower_strings_intro_title",
                           "event_feed_manpower_strings_intro_primary_detail",
                           "event_feed_manpower_strings_intro_secondary_detail")
end

local function createRegionPopulationElements()
        local parent_uic = UIComponent( scripting.m_root:Find("settlement_panel") )
        local header_uic = UIComponent( scripting.m_root:Find("main_settlement_panel_header") )
        
        local original_growth_parent_uic = UIComponent( header_uic:Find("growth_parent") )
        local x, y = original_growth_parent_uic:Position()

        local createPopulationElement = function(componentName, xShift, yShift)
            local uic = ui.createUIComponent(parent_uic, "growth_parent", componentName, "ui/campaign ui/settlement_panel")
            local growth_parent_uic = UIComponent(uic:Find("growth_parent"))

            ui.moveUIC(growth_parent_uic, x + xShift, y + yShift)
            ui.removeUIComponent(uic:Find("growth_bar"))
        end

        createPopulationElement("population1_parent", 62, 3)
        createPopulationElement("population2_parent", 357, 3)
        createPopulationElement("population3_parent", 597, 3)
end

local function moveProvinceHeaderUp()
    local settlement_uic = UIComponent( scripting.m_root:Find("settlement_panel") )
    local header_uic = UIComponent( scripting.m_root:Find("main_settlement_panel_header") )
    
    local move_uic = nil
    
    move_uic = UIComponent(header_uic:Find("dy_province_name"))
    ui.shiftUIC(move_uic, 0, -20)
    
    -- We must also move the dockers up so they expand above the top of the province header
    move_uic = UIComponent(settlement_uic:Find("building_browser_docker"))
    ui.shiftUIC(move_uic, 0, -20)
        
    move_uic = UIComponent(settlement_uic:Find("garrison_docker"))
    ui.shiftUIC(move_uic, 0, -20)
end

local function moveGrowthBarToProvinceHeader()
    local province_name_uic = UIComponent( scripting.m_root:Find("dy_province_name") )
    local province_pX, province_pY = province_name_uic:Position()
    
    local growth_parent_uic = UIComponent( scripting.m_root:Find("growth_parent") )
    ui.moveUIC(growth_parent_uic, province_pX + 22, province_pY + 12)
end

local function moveDevelopmentPointsText()
    local growth_parent_uic = UIComponent( scripting.m_root:Find("growth_parent") )
    local growth_icon_uic = UIComponent( growth_parent_uic:Find("dev_points icon") )
    local growth_number_uic = UIComponent( growth_icon_uic:Find("province_dev_points") )
    
    -- Move it to make up for the resizing within the binary files
    --(since resizing via script doesn't work for text :( )
    ui.shiftUIC(growth_number_uic, -12, 0)
end

-- Pad with spaces so that the province name centers towards the right, instead of the centre
-- (as the growth UI stuff is taking the space to the left)
-- We can't move the text seperately from the header, so we need to do this padding - sadly this causes
-- a slight delay where the province name "snaps" back to the original position after a settlement is selected
-- Perhaps we'll need to find a different UI layout? Or maybe manually pad the province strings in the db, so there's no delay?
-- Or figure out more about UIC format to wrap title in container?
local function padProvinceName()
    local province_name_uic = UIComponent( scripting.m_root:Find("dy_province_name") )
    local province_name = province_name_uic:GetStateText()
    
    province_name_uic:SetStateText("              " .. province_name)
end

-- Gets the current regions showing on the UI
local function getCurrentRegions()
    -- Get the current province from the selected settlement
    local provinceKey = provinces.fromRegionKey[m_currentRegionKey]
    
    -- And get the ui-ordered regions within this province
    local regionsList = regions.fromProvinceKey[provinceKey]
    
    return regionsList
end

-- Temporary UI stuff. Todo, figure out nice way of displaying class populations nicely too
local function createPopulationTooltip(popSourceKey)
    local tooltip = "Manpower:\n\n"
    local popSource = pop.UIgetPopulations(popSourceKey)
    local yellow = ui.textColourer(255, 204, 51)

    for cultureKey, culture in pairs(popSource.cultures) do
        local cultureString = culture:getDisplayName() .. ": " .. yellow(culture:getTotalPopulation()) .. "\n"
        tooltip = tooltip .. cultureString
    end

    return tooltip
end

local function updateGrowthValues(regionList)
    local main_settlement_panel_uic = UIComponent( scripting.m_root:Find("main_settlement_panel") )
    
    local populationUIComponentList = {}
    table.insert(populationUIComponentList, UIComponent( scripting.m_root:Find("population1_parent") ))
    table.insert(populationUIComponentList, UIComponent( scripting.m_root:Find("population2_parent") ))
    table.insert(populationUIComponentList, UIComponent( scripting.m_root:Find("population3_parent") ))

    if #regionList ~= #populationUIComponentList then
        dev.log("ERROR: updateGrowthValues()\n    UI component to region list mismatch. Printing regionList:")
        util.deepPrintTable(regionList, "regionList")
    end

    for i = 1, #regionList do
        local regionKey = regionList[i]

        local population_parent_uic = populationUIComponentList[i]
        local population_number_uic = UIComponent( population_parent_uic:Find("province_dev_points") )
        
        if util_region.isOwnedRegion(regionKey, util_faction.getCurrentFactionKey()) then
            local totalPopulation = tostring(pop.UIgetTotalPopulation(regionKey))
            -- Pad string with spaces to make it left-aligned instead of centre-aligned,
            -- and to stop the text size changing with population
            local NUM_CHARS = 5
            totalPopulation = totalPopulation .. string.rep(' ', NUM_CHARS - totalPopulation)
            
            local tooltip = createPopulationTooltip(regionKey)

            population_number_uic:SetStateText(totalPopulation)
            population_number_uic:SetTooltipText(tooltip)
        else
            population_number_uic:SetStateText("?")
            population_number_uic:SetTooltipText("Manpower Unknown")
        end
    end
end

FACTION_RECRUITABLE_TOOLTIP_REMOVE = "\n\nLeft%-click to recruit this unit, right%-click for further information."

-- TEXT_SEPARATOR is a non-printed character used so we can determine if this tooltip has already been modified
-- and if so, reset the values instead of just appending
TEXT_SEPARATOR = "\030"

-- Trims away unwanted text and adds non-printed marker to show where the modifed text starts
local function trimUnitCardTooltip(recruitCard_uic)
    local text = recruitCard_uic:GetTooltipText()
    -- Check if the tooltip text has been modified by us yet or not
    local separatorPosition = string.find(text, TEXT_SEPARATOR)
    if separatorPosition then
        -- The tooltip has been modified!
        -- Only keep text before the separator
        text = string.sub(text, 1, separatorPosition - 1)
    else
        -- The tooltip hasn't been modified before yet
        local recruitableType = ui_recruitment.getRecruitableType(recruitCard_uic:Id())
        
        if recruitableType == "faction" then
            text = string.gsub(text, FACTION_RECRUITABLE_TOOLTIP_REMOVE, "")
        end
    end
    
    -- Add non-printable character to we can tell where the text is appended
    text = text .. TEXT_SEPARATOR
    return text
end

local function setEnabledUnitCardTooltips(recruitCard_uic, regionPopulation)    
    local unitKey = ui_recruitment.getUnitKeyFromRecruitable(recruitCard_uic:Id())
    local unitSize = unit_interface.getUnitSize(unitKey)

    local text = trimUnitCardTooltip(recruitCard_uic)
    local green = ui.textColourer(75, 174, 77)
    local yellow = ui.textColourer(255, 204, 51)

    -- Add the manpower text
    text = text .. "\n\nRequired manpower: " .. green( unitSize .. " " .. unit_interface.getUnitClass(unitKey) )
    text = text .. "\nRegion manpower: " .. yellow( regionPopulation .. " " .. unit_interface.getUnitClass(unitKey) )

    recruitCard_uic:SetTooltipText(text)
end

local function setDisabledUnitCardTooltips(recruitCard_uic, regionPopulation)
    local unitKey = ui_recruitment.getUnitKeyFromRecruitable(recruitCard_uic:Id())
    local unitSize = unit_interface.getUnitSize(unitKey)

    local text = trimUnitCardTooltip(recruitCard_uic)
    local red = ui.textColourer(214, 37, 38)
    local yellow = ui.textColourer(255, 204, 51)

    -- Add the manpower text
    text = text .. "\n\nRequired manpower: " .. red( unitSize .. " " .. unit_interface.getUnitClass(unitKey) )
    text = text .. "\nRegion manpower: " .. yellow( regionPopulation .. " " .. unit_interface.getUnitClass(unitKey) )

    recruitCard_uic:SetTooltipText(text)
end

function enableRecruitment(recruitCard_uic, popSourceKey)
    local unitKey = ui_recruitment.getUnitKeyFromRecruitable(recruitCard_uic:Id())
    local manpower = pop.UIgetManpowerForUnit(popSourceKey, unitKey)

    ui_recruitment.enableUnitCard(recruitCard_uic)
    setEnabledUnitCardTooltips(recruitCard_uic, manpower)
end

function disableRecruitment(recruitCard_uic, popSourceKey)
    local unitKey = ui_recruitment.getUnitKeyFromRecruitable(recruitCard_uic:Id())
    local manpower = pop.UIgetManpowerForUnit(popSourceKey, unitKey)

    ui_recruitment.disableUnitCard(recruitCard_uic)
    setDisabledUnitCardTooltips(recruitCard_uic, manpower)
end

local function TimeTrigger(context)
    if context.string == "settlementSelected" then
        padProvinceName()
    
        local regionList = getCurrentRegions()
        updateGrowthValues(regionList)
    end
end

------------------------------------------------
---------------- Callbacks ----------------
------------------------------------------------

events.AddEventCallBack("TimeTrigger", TimeTrigger)

events.AddEventCallBack("SettlementSelected", function(context)
    m_currentRegionKey = context:garrison_residence():region():name()
    scripting.game_interface:add_time_trigger("settlementSelected", 0.0) -- Must delay this to after the game updates the region panel, next frame
end)

events.AddEventCallBack("PanelOpenedCampaign", function(context) -- These functions modify UI, so must be called only after the panels are created
    if context.string == "settlement_panel" then
        -- The population elements are created relative to the growth parent uic, so this must be called first until we change this
        createRegionPopulationElements()
        
        moveProvinceHeaderUp()
        moveGrowthBarToProvinceHeader()
        moveDevelopmentPointsText()
    end
end)

-- Logging
dev.log("manpower.ui.lua loaded")