--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- unit_interface.lua
-- Interfaces with the unit data structures

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local lib_unit = require "scripts_libs_mod.lib_manpower_unit_counts"

local alreadyAsserted = {}
local function unitKeyDefined(unitKey)
    if not lib_unit.units[unitKey] then
        if not alreadyAsserted[unitKey] then
            alreadyAsserted[unitKey] = true
            dev.log("ERROR: unitKeyDefined()\n    '"..tostring(unitKey).. "' undefined in lib_manpower_unit_counts.lua")
        end
        return false
    end

    return true
end

function getUnitSize(unitKey)
    if unitKeyDefined(unitKey) then
        return lib_unit.units[unitKey].count
    else
        return 0
    end
end

function getUnitClass(unitKey)
    if unitKeyDefined(unitKey) then
        return lib_unit.units[unitKey].class
    else
        return "class_undefined"
    end
end

function getUnitCulture(unitKey)
    if unitKeyDefined(unitKey) then
        return lib_unit.units[unitKey].culture
    else
        return "culture_undefined"
    end
end

-- Returns whether a unit key is a mercenary
function isMercenary(unitKey)
    if string.find(unitKey, "att_merc_") ~= nil then
        return true
    else
        return false
    end
end

-- Logging
dev.log("manpower.unit_interface.lua loaded")