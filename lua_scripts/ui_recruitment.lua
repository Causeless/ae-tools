--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- ui_recruitment.lua
-- Recruitment UI utility functions

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"

-- Returns a recruitment cards type - mercenary, levy, or faction (normal)
-- Note, this doesn't work with a unit key, only a recruitable card key!
-- As far as the game is concerned, an already recruited levy is identical to a normal unit
function getRecruitableType(cardKey)
    if string.find(cardKey, "_recruitable") then
        return "faction" -- It's just a normal recruitable unit
    elseif string.find(cardKey, "att_merc_") then
        return "mercenary" -- If it starts with att_merc, it's a mercenary. These don't take population
    elseif string.find(cardKey, "_mercenary") then
        return "levy" -- If it doesn't start with att_merc, but ends with _mercenary, it's a levy.
    else
        dev.log("ERROR: getRecruitableType()\n    '"..tostring(cardKey).."' not a recruitment card")
        return "not_a_unit"
    end
end

-- Returns a whether a UIC id is a recruitment card
function isRecruitmentCard(cardKey)
    return string.find(cardKey, "_recruitable") or 
           string.find(cardKey, "_mercenary")
end

-- Returns a whether a UIC id is a recruitment queued card
function isQueuedRecruitmentCard(cardKey)
    return string.find(cardKey, "QueuedLandUnit ") or
           string.find(cardKey, "temp_merc_") 
end

-- Returns the unit key from a recruitment card
function getUnitKeyFromRecruitable(cardKey)
    local unitKey = ""
    
    local recruitableType = getRecruitableType(cardKey)
    if recruitableType == "faction" then
        unitKey = string.gsub(cardKey, "_recruitable", "")
    elseif recruitableType == "mercenary" or recruitableType == "levy" then
        unitKey = string.gsub(cardKey, "_mercenary", "")
    end
    
    return unitKey
end

-- This enable function is usually not needed, as most actions reset the recruitment UI anyways
-- However, this can be used to force enable
function enableUnitCard(recruitCard_uic)
    local clip_uic = UIComponent( recruitCard_uic:Find("unit_clip") )
    local icon_uic = UIComponent( clip_uic:Find("unit_icon") )
    
    recruitCard_uic:SetState("active")
    icon_uic:SetState("active")
end

-- Note that for whatever reason this method of disabling unit cards doesn't work with mercs/levies
-- Must disable normally for that
function disableUnitCard(recruitCard_uic)
    local clip_uic = UIComponent( recruitCard_uic:Find("unit_clip") )
    local icon_uic = UIComponent( clip_uic:Find("unit_icon") )
    
    recruitCard_uic:SetState("inactive")
    icon_uic:SetState("inactive")
end

-- Callbacks
--events.AddEventCallBack("ComponentLClickUp", function(context) output_uicomponent(UIComponent(context.component)) end)

dev.log("ui_recruitment.lua loaded")