--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- force.lua
-- Lib stuff and functions for forces

-- Ideally we'd want to move a lot of manpower.force_state_manager.lua to here
-- But that first requires making generic libs for unit sizes etc

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local util = require "lua_scripts.util"
local events = require "lua_scripts.custom_events"

-- Tracks for how many turns an force has been in foreign regions
m_forceTurnsInForeignRegion = {}

function isForceInOwnedRegion(forceCQI)
    local force = cm:model():military_force_for_command_queue_index(forceCQI)
    local char = force:general_character()

    return char:has_region() and (char:region():owning_faction():name() == char:faction():name())
end

function isForceInForeignRegion(forceCQI)
    return not isForceInOwnedRegion(forceCQI)
end

function getForceTurnsInForeignRegion(forceCQI)
    -- This'll be nil if the force's faction hasn't had a turn yet (a new campaign), or the force is new
    -- Therefore the "or 0"
    return m_forceTurnsInForeignRegion[forceCQI] or 0
end

local function updateForceTurnsInForeignRegion(forceCQI)
    if isForceInOwnedRegion(forceCQI) then
        m_forceTurnsInForeignRegion[forceCQI] = 0
    else -- Force must be in foreign region
        m_forceTurnsInForeignRegion[forceCQI] = getForceTurnsInForeignRegion(forceCQI) + 1
    end
end

local function FactionTurnEnd(context)
    local forces = context:faction():military_force_list():num_items()
    for i = 0, forces - 1 do
        local force = context:faction():military_force_list():item_at(i)
        local forceCQI = force:command_queue_index()
        
        updateForceTurnsInForeignRegion(forceCQI)
    end
    
    for forceCQI, turns in pairs(m_forceTurnsInForeignRegion) do
        local forceExists = cm:model():has_military_force_command_queue_index(forceCQI)
        if not forceExists then
            -- Force was disbanded
            m_forceTurnsInForeignRegion[forceCQI] = nil
        end
    end
end

------------------------------------------------
---------------- Callbacks ----------------
------------------------------------------------

events.AddEventCallBack("FactionTurnEnd", FactionTurnEnd)

------------------------------------------------
---------------- Saving/Loading ----------------
------------------------------------------------

cm:register_loading_game_callback(function(context) 
    m_forceTurnsInForeignRegion = util.deepLoadTable(context, "m_forceTurnsInForeignRegion") 
end)

cm:register_saving_game_callback(function(context) 
    util.deepSaveTable(context, m_forceTurnsInForeignRegion, "m_forceTurnsInForeignRegion")    
end)

-- Logging
dev.log("force.lua loaded")