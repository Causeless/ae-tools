-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"

local function onBuildingCompleted(context)
	local buildingName = context:building():name()
	
    if buildingName == "ai_reg_roman_socii_minor_1"
    or buildingName == "ai_reg_roman_socii_minor_2a"
    or buildingName == "ai_reg_roman_socii_minor_2b"
    or buildingName == "ai_reg_roman_socii_minor_2c"
    or buildingName == "ai_reg_roman_socii_minor_2d" then
        cm:apply_effect_bundle("att_faction_trait_roman", "rom_rome", -1)
    end
end

events.AddEventCallBack("BuildingCompleted", onBuildingCompleted)

dev.log("gov_sys loaded")