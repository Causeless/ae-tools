--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- faction.lua
-- Lib stuff and functions for factions

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"

m_currentFactionKey = nil -- Stores which faction is taking it's turn

local function FactionTurnStart(context)
    m_currentFactionKey = context:faction():name()
end

local function FactionTurnEnd(context)
    m_currentFactionKey = nil
end

function getCurrentFaction()
    return get_faction(m_currentFactionKey)
end

function getCurrentFactionKey()
    return m_currentFactionKey
end

function isDuringFactionTurn()
    return m_currentFactionKey ~= nil and get_faction(m_currentFactionKey);
end

events.AddEventCallBack("FactionTurnStart", FactionTurnStart)
events.AddEventCallBack("FactionTurnEnd", FactionTurnEnd)

-- Saving/loading
cm:register_loading_game_callback(
    function(context)
        m_currentFactionKey = cm:load_value("m_currentFactionKey", "", context)
    end
)

cm:register_saving_game_callback(
    function(context)
        cm:save_value("m_currentFactionKey", m_currentFactionKey, context)
    end 
)

-- Logging
dev.log("faction.lua loaded")