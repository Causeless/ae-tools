--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- main.lua
-- Handles removing growth bar / development points

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local scripting = require "lua_scripts.EpisodicScripting"
local events = require "lua_scripts.custom_events"
local ui = require "lua_scripts.ui"

local function hideGrowthParent()
    local growth_parent_uic = UIComponent( scripting.m_root:Find("growth_parent") )
    ui.moveUIC(growth_parent_uic, -200000, -200000)
end

events.AddEventCallBack("PanelOpenedCampaign", function(context)
    if context.string == "settlement_panel" then
        hideGrowthParent()
    end
end)

-- Logging
dev.log("remove_growth_bar loaded")