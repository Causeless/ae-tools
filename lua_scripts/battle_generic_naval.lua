-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--
--    GENERIC NAVAL BATTLE SCRIPT
--    for non-specific naval battles in the campaign
--
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

-- This generic campaign script, in theory, will allow us to script every battle that happens in a campaign, adding the potential to add new features etc
-- However, this will not carry into custom battles.

----------------------------------------------
--
--  Startup
--
----------------------------------------------



-- clear out loaded files
system.ClearRequiredFiles();

-- load in battle script library
require "lua_scripts.Battle_Script_Header";

-- declare battlemanager object
bm = battle_manager:new(empire_battle:new());
cam = bm:camera();

battle_name = "Generic Naval Battle";

bm:out("==============");
bm:out("==============");
bm:out("==============");
bm:out("Script started: " .. battle_name);
bm:out("==============");

-- register functions to be called on phase changes
bm:register_phase_change_callback("Deployment", function() Deployment_Phase() end);            -- optional deployment phase callback
bm:register_phase_change_callback("Deployed", function() Start_Battle() end);


----------------------------------------------
--
--  Declarations
--
----------------------------------------------

Alliances = bm:alliances();
Local_Alliance = bm:local_alliance();
Alliance_Vgt = Alliances:item(Local_Alliance);

if Local_Alliance == 1 then
    Alliance_AI = Alliances:item(2);
else
    Alliance_AI = Alliances:item(1);
end;

Army_AI_01 = Alliance_AI:armies():item(1);

----------------------------------------------
--
--  Main 
--
----------------------------------------------


function Deployment_Phase()
    bm:out("Battle in deployment phase");
end;



function Start_Battle()
    -- don't close advisor window by default
    -- bm:dont_close_queue_advice();
    
    -- start rout manager
    -- bm:start_rout_manager(1);
        
    -- register victory and defeat VO callbacks
    bm:setup_victory_callback(function() Battle_Is_Ending() end);
        
    -- watch enemy general (if there is one)
    if Army_AI_01:is_commander_alive() then
        bm:out("Enemy commander is alive, listening for him dying");
    else
        bm:out("No enemy commander alive.");
    end;
end;

----------------------------------------------
--
--  Battle end
--
----------------------------------------------


BOOL_Battle_Is_Ending = false;

-- the game has gone to victory countdown phase, someone has won
function Battle_Is_Ending()
    if not BOOL_Battle_Is_Ending then
        BOOL_Battle_Is_Ending = true;
        
        bm:remove_process("General_Watch");
                    
        if is_routing_or_dead(Alliance_Vgt) then    
            bm:out("Player has lost, army is routing");
            
            --bm:stop_advisor_queue();
            --bm:callback(function() bm:end_battle() end, 10000);    
        else
            bm:out("Player has won !");
            
            --bm:stop_advisor_queue();
            --bm:callback(function() bm:end_battle() end, 10000);
        end;
    end;
end;