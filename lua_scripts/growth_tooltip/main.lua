--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- main.lua
-- Handles growth tooltip UI modificatoins

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local scripting = require "lua_scripts.EpisodicScripting"
local events = require "lua_scripts.custom_events"

function modifyGrowthTooltip()
    -- This code finds the growth rate, current growth points, and required growth
    local growth_panel_uic = UIComponent( scripting.m_root:Find("campaign_growth_tooltip") )
    
    if growth_panel_uic then
        local growth_tooltip_uic = UIComponent( growth_panel_uic:Find("tooltip") )

        local current_growth_rate_uic = UIComponent( growth_tooltip_uic:Find("current_growth_rate") )
        local current_growth_rate_title_uic = UIComponent( current_growth_rate_uic:Find("title") )
        local current_growth_rate_value_uic = UIComponent( current_growth_rate_uic:Find("value") )
        
        local factors_total_uic = UIComponent( growth_tooltip_uic:Find("factors_total") )
        local factors_total_title_uic = UIComponent( factors_total_uic:Find("title") )
        local factors_total_value_uic = UIComponent( factors_total_uic:Find("value") )
        local growthFactors = tonumber( factors_total_value_uic:GetStateText() )
        
        -- We replace current_growth_rate, as it's essentially redundant, so we "slide down" the other components over it
        -- This frees a space at the end that we can use
        
        -- We need to put it in a variable beforehand here, otherwise the game crashes
        -- No idea why...
        local tempTitle = factors_total_title_uic:GetStateText()
        local tempValue = factors_total_value_uic:GetStateText()
        current_growth_rate_title_uic:SetStateText(tempTitle)
        current_growth_rate_value_uic:SetStateText(tempValue)
        
        local accumalated_growth_points_uic = UIComponent( growth_tooltip_uic:Find("accumalated_growth_points") ) -- intentional misspelling, like that in-game
        local accumalated_growth_points_title_uic = UIComponent( accumalated_growth_points_uic:Find("title") )
        local accumalated_growth_points_value_uic = UIComponent( accumalated_growth_points_uic:Find("value") )
        local accumulatedGrowth = tonumber( accumalated_growth_points_value_uic:GetStateText() )
        
        tempTitle = accumalated_growth_points_title_uic:GetStateText()
        tempValue = accumalated_growth_points_value_uic:GetStateText()
        factors_total_title_uic:SetStateText(tempTitle)
        factors_total_value_uic:SetStateText(tempValue)
        
        local growth_for_next_points_uic = UIComponent( growth_tooltip_uic:Find("growth_for_next_points") )
        local growth_for_next_points_title_uic = UIComponent( growth_for_next_points_uic:Find("title") )
        local growth_for_next_points_value_uic = UIComponent( growth_for_next_points_uic:Find("value") )
        local requiredGrowth = tonumber( growth_for_next_points_value_uic:GetStateText() )
        
        tempTitle = growth_for_next_points_title_uic:GetStateText()
        tempValue = growth_for_next_points_value_uic:GetStateText()
        accumalated_growth_points_title_uic:SetStateText(tempTitle)
        accumalated_growth_points_value_uic:SetStateText(tempValue)
        
        -- Now we can replace the last with whatever we want
        local turnsUntilDevelopmentPoint
        local readValue
        if growthFactors == 0 then -- Uh oh, divide by 0!
            readValue = "∞" -- There will never be a population surplus :(
        else
            turnsUntilDevelopmentPoint = (requiredGrowth - accumulatedGrowth) / growthFactors
            readValue = tostring(math.ceil(turnsUntilDevelopmentPoint))
        end

        growth_for_next_points_title_uic:SetStateText("Predicted turns for population surplus")
        growth_for_next_points_value_uic:SetStateText(readValue)
    end
end

-- Callbacks
events.AddEventCallBack("ComponentMouseOn", modifyGrowthTooltip)

dev.log("growth_tooltip loaded")