--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- condition.lua
-- Conditions check for a user condition every tick
-- And if the condition is true, it calls some user-provided callback and deletes itself

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"

_activeConditions = {}

function addCondition(condition, callback)
    table.insert(_activeConditions, {["condition"] = condition, ["callback"] = callback})
end

local function checkConditions()
    local trueConditions= {}

    for key, value in pairs(_activeConditions) do
        if value.condition() then
            trueConditions[key] = true
        end
    end

    for key, value in pairs(trueConditions) do
        -- First call the condition
        _activeConditions[key].callback()

        -- Then delete it
        _activeConditions[key] = nil
    end
end

-- Callbacks
events.AddEventCallBack("WorldCreated", function() 
    events.AddEventCallBack("SimTick", checkConditions)
end)

-- Logging
dev.log("condition.lua loaded")