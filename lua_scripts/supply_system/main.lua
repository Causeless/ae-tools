--[[
	SUPPLY SYSTEM
	Created by Niklas: 28/05/2017
	Last Updated: 05/06/2017 
--]]

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"
local supply_manager = require "lua_scripts.supply_system.supply_manager"
local lib_supply_system = require "scripts_libs_mod.lib_supply_system"

-- Tables and Lists
world_effects = lib_supply_system.world_effects
army_stance_effects = lib_supply_system.army_stance_effects
navy_stance_effects = lib_supply_system.navy_stance_effects
battle_effects = lib_supply_system.battle_effects
event_effects = lib_supply_system.event_effects

-- Every turn forces lose/gain supplies because of their environment
function upkeep_supply(context)
	local faction = context:faction()
	local force_list = faction:military_force_list()	
	
	for i = 0, force_list:num_items() - 1 do 
		local force = force_list:item_at(i)
		local character = force:general_character()
		if not supply_manager.is_eligible_force(character) then return end
		
		if force:is_army() then 
			stance_effects = army_stance_effects
		else
			stance_effects = navy_stance_effects
		end

		used_supplies = world_effects[cm:model():season()] + stance_effects[force:active_stance()]	
		
		if character:has_region() then
			local region_faction = character:region():owning_faction():name()
			local character_faction = character:faction():name()
			
			if region_faction == character_faction then
				used_supplies = used_supplies + world_effects["in_own_region"]
			elseif region_faction ~= character_faction then
				used_supplies = used_supplies + world_effects["in_enemy_region"]
			end
		else 
			used_supplies = used_supplies + world_effects["at_sea"] 
		end
		
		if character:is_besieging() then
			used_supplies = used_supplies + stance_effects["MILITARY_FORCE_SITUATIONAL_STANCE_LAY_SIEGE"]
		elseif character:is_blockading() then
			used_supplies = used_supplies + stance_effects["MILITARY_FORCE_SITUATIONAL_STANCE_BLOCKADE"]

		elseif force:has_garrison_residence() then
			if force:garrison_residence():is_under_siege() then
				if force:is_army() then
					used_supplies = used_supplies + stance_effects["MILITARY_FORCE_SITUATIONAL_STANCE_GARRISON"]
				else
					used_supplies = used_supplies + stance_effects["MILITARY_FORCE_SITUATIONAL_STANCE_DOCK"]
				end
			else
				used_supplies = used_supplies + stance_effects[force:active_stance()]
			end
		end

		supply_manager.apply_supply(character, used_supplies)
	end
end

-- After a battle forces lose/gain supplies
function battle_supply(context)
	local character = context:character()
	local battle = context:pending_battle()
	local attacker_result = battle:attacker_battle_result()
	local defender_result = battle:defender_battle_result()
	
	if attacker_result == "close_defeat" and defender_result == "close_defeat" then return end
	
	if character == battle:attacker() then
		result = attacker_result
	elseif character == battle:defender() then
		result = defender_result
	end
	supply_manager.apply_supply(context, battle_effects[result])
end

-- Event effects
function event_supply(context, event)
	local effect = event_effects[event]
	local character = context:character()
	if not supply_manager.is_eligible_force(character) then return end
	supply_manager.apply_supply(character, effect)
end


-- Callbacks
events.AddEventCallBack("WorldCreated", supply_manager.init_supply)
events.AddEventCallBack("FactionTurnStart", upkeep_supply)
events.AddEventCallBack("FactionRoundStart", supply_manager.clean_supply)
events.AddEventCallBack("CharacterCompletedBattle", battle_supply)

events.AddEventCallBack("CharacterPerformsOccupationDecisionSack", function(context)
	event_supply(context, "CharacterPerformsOccupationDecisionSack")
end)

events.AddEventCallBack("CharacterPerformsOccupationDecisionRaze", function(context)
	event_supply(context, "CharacterPerformsOccupationDecisionRaze")
end)

events.AddEventCallBack("CharacterPerformsOccupationDecisionLoot", function(context)
	event_supply(context, "CharacterPerformsOccupationDecisionLoot")
end)

events.AddEventCallBack("CharacterPerformsOccupationDecisionOccupy", function(context)
	event_supply(context, "CharacterPerformsOccupationDecisionOccupy")
end)

events.AddEventCallBack("CharacterPerformsOccupationDecisionResettle", function(context)
	event_supply(context, "CharacterPerformsOccupationDecisionResettle")
end)

events.AddEventCallBack("CharacterEmbarksNavy", function(context)
	event_supply(context, "CharacterEmbarksNavy")
end)

-- Logging
dev.log("force_supply loaded")