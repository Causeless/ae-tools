--[[
	SUPPLY SYSTEM
	Created by Niklas: 28/05/2017
	Last Updated: 05/06/2017 
	mn
	nn
--]]

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"
local lib_supply_manager = require "scripts_libs_mod.lib_supply_manager"

-- Saved/Loaded values
s_newGame = true
force_supply = {}

-- Tables and Lists
supply_effects_army = lib_supply_manager.supply_effects_army
supply_effects_navy = lib_supply_manager.supply_effects_navy

-- Initializes the supply system
function init_supply(context)
	if not s_newGame then return end s_newGame = false
	local faction_list = context:world():faction_list()

	for i = 0, faction_list:num_items() - 1 do
		local force_list = faction_list:item_at(i):military_force_list()
		if force_list:is_empty() then return end
		for i = 0, force_list:num_items() - 1 do
			local force = force_list:item_at(i)
			local character = force:general_character()
			if not is_eligible_force(character) then return end
			local character_cqi = character:cqi()
			local supply_effects = supply_type(force)
			cm:apply_effect_bundle_to_characters_force(supply_effects[#supply_effects], character_cqi, 0, true)
		end
	end
end

-- Applies change of the supply level to a force or refills it entirely
function apply_supply(character, used_supplies)
	local character_cqi = character:cqi()
	local force = character:military_force()
	local force_cqi = force:command_queue_index()
	local supply_effects = supply_type(force)
			
	if force_supply[force_cqi] == nil or used_supplies == "refill" then
		force_supply[force_cqi] = #supply_effects
	elseif (force_supply[force_cqi] + used_supplies) > #supply_effects then
		force_supply[force_cqi] = #supply_effects
	elseif (force_supply[force_cqi] + used_supplies) < 1 then
		force_supply[force_cqi] = 1
	else
		force_supply[force_cqi] = force_supply[force_cqi] + used_supplies
	end
	
	for i = 1, #supply_effects do
		cm:remove_effect_bundle_from_characters_force(supply_effects[i], character_cqi)
	end
	cm:apply_effect_bundle_to_characters_force(supply_effects[force_supply[force_cqi]], character_cqi, 0, true)
end

-- Cleaning the table
function clean_supply()
	for force_cqi in pairs(force_supply) do 
		if not cm:model():has_military_force_command_queue_index(force_cqi) then
			force_supply[force_cqi] = nil
		end
	end
end

-- Check for supply type
function supply_type(force)
	if force:is_army() then 
		return supply_effects_army
	else
		return supply_effects_navy
	end
end

-- Checks for eligible forces
function is_eligible_force(character)
	if character:is_null_interface()
	or character:faction():is_horde()
	and not character:character_type("general") then 
		return false
	end
	return true
end

-- Saving/Loading
cm:register_loading_game_callback(
    function(context)
		s_newGame = cm:load_value("s_newGame", true, context)
		force_supply = util.deepLoadTable(context, "force_supply")
    end 
)

cm:register_saving_game_callback(
    function(context)
		cm:save_value("s_newGame", s_newGame, context)
		util.deepSaveTable(context, force_supply, "force_supply")
    end 
)