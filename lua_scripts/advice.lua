--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- advice.lua
-- Logic to show advice upon character/settlement selected

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local world = require "lua_scripts.world"
local util = require "lua_scripts.util"
local events = require "lua_scripts.custom_events"

local m_charAdvicePlayed = {}
local m_regionAdvicePlayed = {}

function showAdviceCharacter(context)
    local character = context:character()
    local cqi = character:cqi()
    
    if not world.isPlayerTurn() or m_charAdvicePlayed[cqi] then return end
    
    m_charAdvicePlayed[cqi] = true

    cm:show_advice("AE.Advice.Char." .. cqi, true)
end

function showAdviceRegion(context)
    local region = context:garrison_residence():region()
    local name = region:name()
    
    if not world.isPlayerTurn() or m_regionAdvicePlayed[name] then return end
    
    m_regionAdvicePlayed[name] = true
    
    cm:show_advice("AE.Advice.Region." .. name, true)
end

-- Callbacks
events.AddEventCallBack("CharacterSelected", showAdviceCharacter)
events.AddEventCallBack("SettlementSelected", showAdviceRegion)

-- Saving/Loading
cm:register_loading_game_callback(
    function(context)
        m_charAdvicePlayed = util.deepLoadTable(context, "m_charAdvicePlayed")
        m_regionAdvicePlayed = util.deepLoadTable(context, "m_regionAdvicePlayed")
    end
)
cm:register_saving_game_callback(
    function(context)
        util.deepSaveTable(context, m_charAdvicePlayed, "m_charAdvicePlayed")
        util.deepSaveTable(context, m_regionAdvicePlayed, "m_regionAdvicePlayed")
    end
)

-- Logging
dev.log("advice.lua loaded")