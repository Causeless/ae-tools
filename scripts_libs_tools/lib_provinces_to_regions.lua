--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- lib_provinces_to_regions.lua
-- Stores table matching a province to the three included regions (in UI order)
-- This is used for manpower, so we can figure out the regions being shown on the UI

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- TODO: We need to add the data here for the other campaign maps!

fromProvinceKey = {
    -- Grand Campaign
    ["att_prov_aegyptus"] = {"att_reg_aegyptus_alexandria", "att_reg_aegyptus_berenice", "att_reg_aegyptus_oxyrhynchus"},
    ["att_prov_aethiopia"] = {"att_reg_aethiopia_aksum", "att_reg_aethiopia_adulis", "att_reg_aethiopia_pachoras"},
    ["att_prov_africa"] = {"att_reg_africa_carthago", "att_reg_africa_hadrumentum", "att_reg_africa_constantina"},
    ["att_prov_aquitania"] = {"att_reg_aquitania_burdigala", "att_reg_aquitania_avaricum", "att_reg_aquitania_elusa"},
    ["att_prov_arabia_felix"] = {"att_reg_arabia_felix_zafar", "att_reg_arabia_felix_omana", "att_reg_arabia_felix_eudaemon"},
    ["att_prov_arabia_magna"] = {"att_reg_arabia_magna_yathrib", "att_reg_arabia_magna_hira", "att_reg_arabia_magna_dumatha"},
    ["att_prov_armenia"] = {"att_reg_armenia_duin", "att_reg_armenia_tosp", "att_reg_armenia_payttakaran"},
    ["att_prov_asia"] = {"att_reg_asia_ephesus", "att_reg_asia_synnada", "att_reg_asia_cyzicus"},
    ["att_prov_asorstan"] = {"att_reg_asorstan_ctesiphon", "att_reg_asorstan_arbela", "att_reg_asorstan_meshan"},
    ["att_prov_baetica"] = {"att_reg_baetica_corduba", "att_reg_baetica_hispalis", "att_reg_baetica_malaca"},
    ["att_prov_belgica"] = {"att_reg_belgica_augusta_treverorum", "att_reg_belgica_durocorterum", "att_reg_belgica_colonia_agrippina"},
    ["att_prov_bithynia"] = {"att_reg_bithynia_ancyra", "att_reg_bithynia_amasea", "att_reg_bithynia_nicomedia"},
    ["att_prov_britannia_inferior"] = {"att_reg_britannia_inferior_eboracum", "att_reg_britannia_inferior_lindum", "att_reg_britannia_inferior_segontium"},
    ["att_prov_britannia_superior"] = {"att_reg_britannia_superior_londinium", "att_reg_britannia_superior_corinium", "att_reg_britannia_superior_camulodunon"},
    ["att_prov_caledonia_et_hibernia"] = {"att_reg_caledonia_et_hibernia_tuesis", "att_reg_caledonia_et_hibernia_eblana", "att_reg_caledonia_et_hibernia_eildon"},
    ["att_prov_cappadocia"] = {"att_reg_cappadocia_caesarea_eusebia", "att_reg_cappadocia_melitene", "att_reg_cappadocia_trapezus"},
    ["att_prov_carthaginensis"] = {"att_reg_carthaginensis_carthago_nova", "att_reg_carthaginensis_segobriga", "att_reg_carthaginensis_toletum"},
    ["att_prov_caucasia"] = {"att_reg_caucasia_mtskheta", "att_reg_caucasia_kotais", "att_reg_caucasia_gabala"},
    ["att_prov_cilicia"] = {"att_reg_cilicia_tarsus", "att_reg_cilicia_iconium", "att_reg_cilicia_myra"},
    ["att_prov_dacia"] = {"att_reg_dacia_apulum", "att_reg_dacia_petrodava", "att_reg_dacia_romula"},
    ["att_prov_dalmatia"] = {"att_reg_dalmatia_salona", "att_reg_dalmatia_siscia", "att_reg_dalmatia_domavia"},
    ["att_prov_dardania"] = {"att_reg_dardania_serdica", "att_reg_dardania_scupi", "att_reg_dardania_viminacium"},
    ["att_prov_frisia"] = {"att_reg_frisia_tulifurdum", "att_reg_frisia_flevum", "att_reg_frisia_angulus"},
    ["att_prov_gallaecia"] = {"att_reg_gallaecia_bracara", "att_reg_gallaecia_asturica", "att_reg_gallaecia_brigantium"},
    ["att_prov_germania"] = {"att_reg_germania_uburzis", "att_reg_germania_aregelia", "att_reg_germania_lupfurdum"},
    ["att_prov_germano_sarmatia"] = {"att_reg_germano_sarmatia_palteskja", "att_reg_germano_sarmatia_duna", "att_reg_germano_sarmatia_oium"},
    ["att_prov_gothiscandza"] = {"att_reg_gothiscandza_gothiscandza", "att_reg_gothiscandza_rugion", "att_reg_gothiscandza_ascaucalis"},
    ["att_prov_hercynia"] = {"att_reg_hercynia_casurgis", "att_reg_hercynia_budorgis", "att_reg_hercynia_nitrahwa"},
    ["att_prov_hyperborea"] = {"att_reg_hyperborea_kariskos", "att_reg_hyperborea_sylis", "att_reg_hyperborea_moramar"},
    ["att_prov_khwarasan"] = {"att_reg_khwarasan_abarshahr", "att_reg_khwarasan_merv", "att_reg_khwarasan_harey"},
    ["att_prov_italia"] = {"att_reg_italia_roma", "att_reg_italia_fiorentia", "att_reg_italia_neapolis"},
    ["att_prov_libya"] = {"att_reg_libya_ptolemais", "att_reg_libya_paraetonium", "att_reg_libya_augila"},
    ["att_prov_liguria"] = {"att_reg_liguria_mediolanum", "att_reg_liguria_genua", "att_reg_liguria_segusio"},
    ["att_prov_lugdunensis"] = {"att_reg_lugdunensis_lugdunum", "att_reg_lugdunensis_turonum", "att_reg_lugdunensis_rotomagus"},
    ["att_prov_lusitania"] = {"att_reg_lusitania_emerita_augusta", "att_reg_lusitania_pax_augusta", "att_reg_lusitania_olisipo"},
    ["att_prov_macedonia"] = {"att_reg_macedonia_thessalonica", "att_reg_macedonia_corinthus", "att_reg_macedonia_dyrrhachium"},
    ["att_prov_magna_graecia"] = {"att_reg_magna_graecia_tarentum", "att_reg_magna_graecia_syracusae", "att_reg_magna_graecia_rhegium"},
    ["att_prov_makran"] = {"att_reg_makran_pura", "att_reg_makran_phra", "att_reg_makran_harmosia"},
    ["att_prov_mauretania"] = {"att_reg_mauretania_caesarea", "att_reg_mauretania_tingis", "att_reg_mauretania_tamousiga"},
    ["att_prov_maxima_sequanorum"] = {"att_reg_maxima_sequanorum_vesontio", "att_reg_maxima_sequanorum_argentoratum", "att_reg_maxima_sequanorum_octodurus"},
    ["att_prov_media_atropatene"] = {"att_reg_media_atropatene_ecbatana", "att_reg_media_atropatene_rhaga", "att_reg_media_atropatene_ganzaga"},
    ["att_prov_mediterraneus_occidentalis"] = {"att_reg_mediterraneus_occidentalis_caralis", "att_reg_mediterraneus_occidentalis_palma", "att_reg_mediterraneus_occidentalis_ajax"},
    ["att_prov_mediterraneus_orientalis"] = {"att_reg_mediterraneus_orientalis_gortyna", "att_reg_mediterraneus_orientalis_constantia", "att_reg_mediterraneus_orientalis_rhodes"},
    ["att_prov_narbonensis"] = {"att_reg_narbonensis_aquae_sextiae", "att_reg_narbonensis_vienna", "att_reg_narbonensis_narbo"},
    ["att_prov_osroene"] = {"att_reg_osroene_edessa", "att_reg_osroene_amida", "att_reg_osroene_nisibis"},
    ["att_prov_palaestinea"] = {"att_reg_palaestinea_nova_trajana_bostra", "att_reg_palaestinea_aila", "att_reg_palaestinea_aelia_capitolina"},
    ["att_prov_pannonia"] = {"att_reg_pannonia_sirmium", "att_reg_pannonia_savaria", "att_reg_pannonia_sopianae"},
    ["att_prov_persis"] = {"att_reg_persis_stakhr", "att_reg_persis_siraf", "att_reg_persis_behdeshir"},
    ["att_prov_phazania"] = {"att_reg_phazania_garama", "att_reg_phazania_dimmidi", "att_reg_phazania_cydamus"},
    ["att_prov_raetia_et_noricum"] = {"att_reg_raetia_et_noricum_augusta_vindelicorum", "att_reg_raetia_et_noricum_iuvavum", "att_reg_raetia_et_noricum_virunum"},
    ["att_prov_sarmatia_asiatica"] = {"att_reg_sarmatia_asiatica_anacopia", "att_reg_sarmatia_asiatica_samandar", "att_reg_sarmatia_asiatica_tanais"},
    ["att_prov_sarmatia_europaea"] = {"att_reg_sarmatia_europaea_chersonessus", "att_reg_sarmatia_europaea_olbia", "att_reg_sarmatia_europaea_gelonus"},
    ["att_prov_scandza"] = {"att_reg_scandza_hrefnesholt", "att_reg_scandza_alabu", "att_reg_scandza_hafn"},
    ["att_prov_scythia"] = {"att_reg_scythia_sarai", "att_reg_scythia_bolghar", "att_reg_scythia_ra"},
    ["att_prov_spahan"] = {"att_reg_spahan_spahan", "att_reg_spahan_issatis", "att_reg_spahan_susa"},
    ["att_prov_syria"] = {"att_reg_syria_antiochia", "att_reg_syria_tyrus", "att_reg_syria_emesa"},
    ["att_prov_tarraconensis"] = {"att_reg_tarraconensis_tarraco", "att_reg_tarraconensis_pompaelo", "att_reg_tarraconensis_caesaraugusta"},
    ["att_prov_thracia"] = {"att_reg_thracia_constantinopolis", "att_reg_thracia_trimontium", "att_reg_thracia_marcianopolis"},
    ["att_prov_transcarpathia"] = {"att_reg_transcarpathia_arheimar", "att_reg_transcarpathia_leopolis", "att_reg_transcarpathia_belz"},
    ["att_prov_transcaspia"] = {"att_reg_transcaspia_kath", "att_reg_transcaspia_siahkuh", "att_reg_transcaspia_dahistan"},
    ["att_prov_tripolitana"] = {"att_reg_tripolitana_leptis_magna", "att_reg_tripolitana_sabrata", "att_reg_tripolitana_macomades"},
    ["att_prov_venetia"] = {"att_reg_venetia_ravenna", "att_reg_venetia_verona", "att_reg_venetia_aquileia"}
}